/**
 * \section LICENSE
 * Copyright@copy;2018 EchoNous, Inc confidential - all rights reserved
 *
 * \section DESCRIPTION
 * tlv host side application
 *
 * \file tlv_host.cpp
 *
 */

#include "tlv_host.h"

using namespace std;

uint8_t buffer[HS_BULK_MAX_PACKET_SIZE];
libusb_device_handle *dev_handle;
libusb_context *ctx = NULL;

char test_string[]= "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";

int usb_open(int vid,int pid)
{
    libusb_device **devs;
    int r;

    r = libusb_init(&ctx);
    if(r < 0)
    {
        cout<<"libusb_init() failed with error" << r << endl;
        return r;
    }

    r = libusb_get_device_list(ctx, &devs);
    if(r < 0)
    {
        cout<< "Get Device Error" << endl;
        return r;
    }
    cout << "Devices in list" << r << endl;

    dev_handle = libusb_open_device_with_vid_pid(ctx, vid, pid);
    if(dev_handle == NULL)
        cout<<"Cannot open device"<<endl;
    else
        cout<<"Device Opened"<<endl;

    libusb_free_device_list(devs, 1);


    /*  find out if kernel driver is attached   */
    if(libusb_kernel_driver_active(dev_handle, 0) == 1)
    {
        cout << "Kernel Driver Active" << endl;
        if(libusb_detach_kernel_driver(dev_handle, 0) == 0)
            cout << "Kernel Driver Detached!" << endl;
    }

    r = libusb_claim_interface(dev_handle, 0);
    if(r < 0)
    {
        cout << "Cannot Claim Interface" << endl;
        return r;
    }
    cout << "Claimed Interface" << endl;
    return r;
}

int usb_close()
{
    int r;
    
    r = libusb_release_interface(dev_handle, 0);
    if(r!=0)
    {
        cout << "Cannot Release Interface" << endl;
        return r;
    }
    cout << "Released Interface" << endl;

    libusb_close(dev_handle);
    libusb_exit(ctx);

    return r;
}

int tlv_send(uint16_t tag, uint16_t length, uint32_t *value)
{
    struct tlv *sent_pkt;
    int byte_sent = 0;
    ssize_t cnt;
    int r;
    
    switch(tag)
    {
        case SINGLE_REG_READ:
            {
                sent_pkt = (struct tlv *)malloc(sizeof(struct tlv));
                memset(sent_pkt , 0 , sizeof(struct tlv));
                sent_pkt->tag = tag;
                sent_pkt->length = length;
                memcpy(&sent_pkt->value[0],value,length*sizeof(uint32_t));
                memcpy(buffer,sent_pkt,sizeof(struct tlv));
        #ifdef TLV_DEBUG
        	    printf("Tag Sent:%x\n",sent_pkt->tag);
        	    printf("Length Sent:%x\n",sent_pkt->length);
                printf("Value Sent:%x\n",sent_pkt->value[0]);
        #endif
                break;
            }
        case SINGLE_REG_WRITE:
            {   
                sent_pkt = (struct tlv *)malloc(sizeof(struct tlv));
                memset(sent_pkt , 0 , sizeof(struct tlv));
                sent_pkt->tag = tag;
                sent_pkt->length = length;
                memcpy(&sent_pkt->value,value,length*sizeof(uint32_t));
                memcpy(buffer,sent_pkt,sizeof(struct tlv));
        #ifdef TLV_DEBUG
        	    printf("Tag Sent:%x\n",sent_pkt->tag);
        	    printf("Length Sent:%x\n",sent_pkt->length);
                printf("Value Sent:%x\n",sent_pkt->value[0]);
        #endif
                break;
            }
        case BLOCK_REG_READ:
            {
                break;
            }
        case BLOCK_REG_WRITE:
            {
                break;
            }
        default:
            {
                sent_pkt = (struct tlv *)malloc(sizeof(struct tlv));
                memset(sent_pkt , 0 , sizeof(struct tlv));
                sent_pkt->tag = tag;
                sent_pkt->length = length;
                memcpy(&sent_pkt->value[0],value,length*sizeof(uint32_t));
                memcpy(buffer,sent_pkt,sizeof(struct tlv));
        #ifdef TLV_DEBUG
        	    printf("Tag Sent:%x\n",sent_pkt->tag);
        	    printf("Length Sent:%x\n",sent_pkt->length);
                printf("Value Sent:%x\n",sent_pkt->value[0]);
        #endif

            }
    
    }
    
    r = libusb_bulk_transfer(dev_handle, Bulk_OUT_EP2, buffer, sizeof(struct tlv), &byte_sent, 0);

    if(r == 0)
        cout<<"Writing Of Data Successful at EP2 Out Endpoint" << endl;
    else
        cout<<"Writing Error at EP2 Out Endpoint" << r << endl;

    free(sent_pkt);
}

int tlv_receive()
{
    struct tlv *recv_frame;
    uint8_t *r_buffer;
    int byte_receive = 0,r;
    recv_frame = (struct tlv *)malloc(sizeof(struct tlv));
    r_buffer = (uint8_t *)malloc(512);
    
    r = libusb_bulk_transfer(dev_handle, Bulk_IN_EP1, r_buffer, 512, &byte_receive, 0);
    if(r == 0)
        cout<<"Reading Of Data Successful at EP1 IN Endpoint" << endl;
    else
        cout<<"Reading Error at EP1 IN Endpoint" << libusb_error_name(r) << endl;

    memcpy(recv_frame, r_buffer, sizeof(struct tlv));
    
#ifdef TLV_DEBUG
    printf("%s: Tag:%x\n",__func__,recv_frame->tag);
    printf("%s: Length:%x\n",__func__,recv_frame->length);
#endif

    switch(recv_frame->tag)
    {
        case SINGLE_REG_READ:
            {
                printf("Single Register Read Response = %x\n",recv_frame->value[0]);
                break;
            }
        case SINGLE_REG_WRITE:
            {   
                break;
            }
        case BLOCK_REG_READ:
            {
                for(int i=0;i<recv_frame->length;i++)
                     printf("BR Value received:%x\n",recv_frame->value[i]);
                break;
            }
        case BLOCK_REG_WRITE:
            {  
                break;
            }
        default:
            {
        	    printf("Wrong tag received\n");
                printf("Error Code = %x\n",recv_frame->value[0]);
            }
    }

    free(recv_frame);
    free(r_buffer);
}

void loopback()
{
    uint8_t *ptr = (uint8_t *)malloc(HS_BULK_MAX_PACKET_SIZE);
    uint8_t *ptr1 = (uint8_t *)malloc(HS_BULK_MAX_PACKET_SIZE);
    int actual,r;

    for(int i=0;i<5;i++)
    {
        memcpy(ptr, test_string, HS_BULK_MAX_PACKET_SIZE);
    
        r = libusb_bulk_transfer(dev_handle,0x04, ptr,HS_BULK_MAX_PACKET_SIZE, &actual, 0);
        if(r == 0)
        {
    
            cout<<"\nSending Data at EP04 :"<< ptr << endl;
        }
        else
        {
            cout<<"\nWriting Error at EP04"<< libusb_error_name(r) << endl;
        }
    
        memset(ptr1, 0, HS_BULK_MAX_PACKET_SIZE);
        r = libusb_bulk_transfer(dev_handle, 0x83, ptr1, HS_BULK_MAX_PACKET_SIZE, &actual, 0);
        if(r == 0)
        {
            cout<<"\nReading Data at EP03: "<<ptr1 << endl;
        }
        else
        {
            cout<<"\nReading Error at EP03"<< libusb_error_name(r) << endl;
        }

        memset(ptr,0,HS_BULK_MAX_PACKET_SIZE);
    }
}

int main(int argc,char *argv[])
{
    int r,ch;

    r = usb_open(VENDOR_ID,PRODUCT_ID);

    if(r < 0)
    {
	    cout << "Can't Open usb device" << endl;
        return -1;
    }

    switch(atoi(argv[1]))
    {
    	case SINGLE_REG_READ:
            {    
                uint32_t reg = FUSB300_OFFSET_IGER0;
                tlv_send(SINGLE_REG_READ, 1, &reg);
                tlv_receive();
                break;
            }
    	case SINGLE_REG_WRITE:
            {
                uint32_t reg[] = {SYS_TCTL_ADDR,atoi(argv[2])};
                tlv_send(SINGLE_REG_WRITE, 2, reg);
                tlv_receive();
                break;
            }
    	case BLOCK_REG_READ:
            {
                break;
            }
    	case BLOCK_REG_WRITE:
            {
                break;
            }
        case LOOPBACK:
            {
                loopback();
                break;
            }
        case ERROR:
            {
                uint32_t reg = SYS_TCTL_ADDR;
                tlv_send(ERROR, 1, &reg);
                tlv_receive();
                break;
            }
    	default:
            {
    		    cout << "Invalid Choice" << endl;
            }
    }

    r = usb_close();
    if(r < 0)
    {
    	cout << "Can't close usb device" << endl;
        return -1;
    }

    return 0;
}
