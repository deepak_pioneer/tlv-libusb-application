/**
 * \section LICENSE
 * Copyright@copy;2018 EchoNous, Inc confidential - all rights reserved
 *
 * \section DESCRIPTION
 * tlv host side application header
 *
 * \file tlv_host.h
 *
 */

#ifndef __TLV_HOST_H__
#define __TLV_HOST_H__

#include <string.h>
#include <iostream>
#include <libusb-1.0/libusb.h>
#include <malloc.h>
#include <stdlib.h>

/* Debug Mode */
#define  TLV_DEBUG

/* Pre-processor Directives */

#define Bulk_OUT_EP2 	0x02
#define Bulk_IN_EP1 	0x81

#define VENDOR_ID       0x1dbf
#define PRODUCT_ID      0x0100

#define SS_BULK_MAX_PACKET_SIZE	    1024
#define HS_BULK_MAX_PACKET_SIZE     512

/* Device Registers */

#define SYS_TCTL_ADDR               0x10a00100               
#define FOTG300_DEV_BASE            0x10302000              /*  FOTG300 Base address    */

#define FUSB300_OFFSET_GCR		    (FOTG300_DEV_BASE + 0x00)
#define FUSB300_OFFSET_GTM		    (FOTG300_DEV_BASE + 0x04)
#define FUSB300_OFFSET_IGER0		(FOTG300_DEV_BASE + 0x420)
#define FUSB300_OFFSET_IGER1		(FOTG300_DEV_BASE + 0x424)
#define FUSB300_OFFSET_FEATURE1		(FOTG300_DEV_BASE + 0x614)
#define FUSB300_OFFSET_FEATURE2		(FOTG300_DEV_BASE + 0x618)


/* Tags */
#define SINGLE_REG_READ     1
#define SINGLE_REG_WRITE    2
#define BLOCK_REG_READ      3
#define BLOCK_REG_WRITE     4
#define LOOPBACK            5
#define ERROR		        7

/* TLV structure */
struct tlv
{
    uint16_t tag;
    uint16_t length;
    uint32_t value[127];
};

/* APIs */

int usb_open(int,int);
int usb_close();
int tlv_send(uint16_t,uint16_t,uint32_t *);
int tlv_receive();
void loopback();


#endif /* __TLV_HOST_H__ */

